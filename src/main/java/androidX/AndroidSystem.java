package androidX;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import androidX.event.EventDispatcher;
import androidX.event.applications.CallRecorderApplication;
import androidX.event.applications.PhoneApplication;
import androidX.event.applications.SMSApplication;
import androidX.event.events.CallEndedEvent;
import androidX.event.events.CallStartedEvent;
import androidX.event.events.OutgoingCallEvent;
import androidX.event.listeners.ICallEndedListener;
import androidX.event.listeners.ICallStartedListener;

/**
 * Created by amen on 9/15/17.
 */
public class AndroidSystem implements ICallEndedListener, ICallStartedListener {
    private List<Notification> listaNotyfikacji;
    private List<Integer> ongoingCalls;

    public AndroidSystem() {
        EventDispatcher.instance.registerObject(this);

        this.listaNotyfikacji = new ArrayList<>();
        this.ongoingCalls = new ArrayList<>();

        new CallRecorderApplication();
        new PhoneApplication();
        new SMSApplication();
    }

    @Override
    public void callEnded(int callId) {
        ongoingCalls.remove((Integer) callId);
        System.out.println("SYS: call_end" + callId);
    }

    @Override
    public void callStarted(int callId) {
        ongoingCalls.add(callId);
        System.out.println("SYS: call_start" + callId);
    }

    @Override
    public void outgoingCallStarted(int callId) {
        if (ongoingCalls.size() < 3) {
            ongoingCalls.add(callId);
            System.out.println("SYS: out_call_start" + callId);
        }
    }

    public void startCall(int id) {
        if (ongoingCalls.size() < 3) {
            EventDispatcher.instance.dispatch(new CallStartedEvent(id));
        } else {
            System.out.println("Too many calls.");
        }
    }

    public void endCall(int id) {
        if (ongoingCalls.contains(id)) {
            System.out.println("Koncze polaczenie o id: " + id);
            EventDispatcher.instance.dispatch(new CallEndedEvent(id));
        } else {
            System.out.println("Nie mogę zakończyć połączenia które nie zostało rozpoczęte.");
        }
    }

    public void outStartCall(int call_id) {
        if (ongoingCalls.size() < 3) {
            EventDispatcher.instance.dispatch(new OutgoingCallEvent(call_id));
        }
    }
}
