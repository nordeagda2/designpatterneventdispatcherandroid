package androidX;

import androidX.event.EventDispatcher;
import androidX.event.SmsMessage;
import androidX.event.events.CallEndedEvent;
import androidX.event.events.CallStartedEvent;
import androidX.event.events.SMSReceivedEvent;

import java.util.Scanner;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by amen on 9/15/17.
 */
public class Main {
    
    public static void main(String[] args) {
        AndroidSystem manager = new AndroidSystem();

        Scanner sc = new Scanner(System.in);
        while (sc.hasNextLine()) {
            String line = sc.nextLine();
            String command = line.split(" ")[0];
            int call_id = Integer.parseInt(line.split(" ")[1]);
            if (command.equalsIgnoreCase("start")) {
//                EventDispatcher.instance.dispatch(new CallStartedEvent(call_id));
                manager.startCall(call_id);
            } else if (command.equalsIgnoreCase("stop")) {
                //                EventDispatcher.instance.dispatch(new CallStartedEvent(call_id));
                manager.endCall(call_id);
            }else if (command.equalsIgnoreCase("startout")) {
                //                EventDispatcher.instance.dispatch(new CallStartedEvent(call_id));
                manager.outStartCall(call_id);
            }else if(command.equalsIgnoreCase("sendsms")){
                String restOfCommand = line.split(" ", 3)[2];
                EventDispatcher.instance.dispatch(new SMSReceivedEvent(new SmsMessage(restOfCommand)));
            }
        }
    }

}
