package androidX;

/**
 * Created by amen on 9/16/17.
 */
public class Notification {
    private String jakiePowiadomienie;

    public Notification(String jakiePowiadomienie) {
        this.jakiePowiadomienie = jakiePowiadomienie;
    }

    public String getJakiePowiadomienie() {
        return jakiePowiadomienie;
    }

    public void setJakiePowiadomienie(String jakiePowiadomienie) {
        this.jakiePowiadomienie = jakiePowiadomienie;
    }
}
