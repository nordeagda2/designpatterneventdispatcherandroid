package androidX.event;

/**
 * Created by amen on 12/17/17.
 */
public class SmsMessage {
    private String message;

    public SmsMessage(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "SmsMessage{" +
                "message='" + message + '\'' +
                '}';
    }
}
