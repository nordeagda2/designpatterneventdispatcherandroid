package androidX.event.events;

import androidX.event.EventDispatcher;
import androidX.event.listeners.ICallEndedListener;
import androidX.event.listeners.ICallStartedListener;

import java.util.List;

/**
 * Created by amen on 9/16/17.
 */
public class CallEndedEvent implements IEvent {
    private int call_id;

    public CallEndedEvent(int call_id) {
        this.call_id = call_id;
    }

    @Override
    public void run() {
        List<ICallEndedListener> listenerList = EventDispatcher.instance.getAllObjectsImplementingInterface(ICallEndedListener.class);
        for (ICallEndedListener listener : listenerList) {
            listener.callEnded(call_id);
        }
    }
}
