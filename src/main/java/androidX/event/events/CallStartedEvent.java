package androidX.event.events;

import androidX.event.EventDispatcher;
import androidX.event.listeners.ICallStartedListener;

import java.util.*;

/**
 * Created by amen on 9/16/17.
 */
public class CallStartedEvent implements IEvent {
    private int call_id;

    public CallStartedEvent(int call_id) {
        this.call_id = call_id;
    }

    @Override
    public void run() {
        List<ICallStartedListener> listenerList = EventDispatcher.instance.getAllObjectsImplementingInterface(ICallStartedListener.class);
        for (ICallStartedListener listener : listenerList) {
            listener.callStarted(call_id);
        }
    }
}
