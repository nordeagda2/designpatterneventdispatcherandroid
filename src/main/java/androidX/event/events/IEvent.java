package androidX.event.events;

/**
 * Created by amen on 9/15/17.
 */
public interface IEvent {
    void run();
}
