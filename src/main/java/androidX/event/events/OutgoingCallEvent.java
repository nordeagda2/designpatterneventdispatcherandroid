package androidX.event.events;

import androidX.event.EventDispatcher;
import androidX.event.listeners.ICallStartedListener;

import java.util.List;

/**
 * Created by amen on 9/18/17.
 */
public class OutgoingCallEvent implements IEvent {

    private int call_id;

    public OutgoingCallEvent(int call_id) {
        this.call_id = call_id;
    }

    @Override
    public void run() {
        List<ICallStartedListener> listenerList = EventDispatcher.instance.getAllObjectsImplementingInterface(ICallStartedListener.class);
        for (ICallStartedListener listener : listenerList) {
            listener.outgoingCallStarted(call_id);
        }
    }
}
