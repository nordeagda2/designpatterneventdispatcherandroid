package androidX.event.events;

import androidX.event.EventDispatcher;
import androidX.event.SmsMessage;
import androidX.event.listeners.ISMSListener;

import java.util.List;

/**
 * Created by amen on 12/17/17.
 */
public class SMSSentEvent implements IEvent {
    private SmsMessage message;

    public SMSSentEvent(SmsMessage message) {
        this.message = message;
    }

    @Override
    public void run() {
        List<ISMSListener> listenerList = EventDispatcher.instance.getAllObjectsImplementingInterface(ISMSListener.class);
        for (ISMSListener listener : listenerList) {
            listener.smsSent(message);
        }
    }
}
