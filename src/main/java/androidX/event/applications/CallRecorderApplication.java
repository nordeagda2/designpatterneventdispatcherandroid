package androidX.event.applications;

import androidX.event.EventDispatcher;
import androidX.event.listeners.ICallEndedListener;
import androidX.event.listeners.ICallStartedListener;

/**
 * Created by amen on 9/15/17.
 */
public class CallRecorderApplication implements ICallEndedListener, ICallStartedListener {

    public CallRecorderApplication(){
        EventDispatcher.instance.registerObject(this);
    }

    @Override
    public void callEnded(int callId) {
        System.out.println("CALLR: call_end" + callId);
    }

    @Override
    public void callStarted(int callId) {
        System.out.println("CALLR: call_start" + callId);
    }

    @Override
    public void outgoingCallStarted(int callId) {
        System.out.println("CALLR: out_call_start" + callId);
    }
}
