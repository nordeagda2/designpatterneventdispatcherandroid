package androidX.event.applications;

import androidX.event.EventDispatcher;
import androidX.event.SmsMessage;
import androidX.event.listeners.ISMSListener;

import java.util.*;
import java.util.ArrayList;

/**
 * Created by amen on 12/17/17.
 */
public class SMSApplication implements ISMSListener {

    private List<SmsMessage> list = new ArrayList<>();

    public SMSApplication() {
        EventDispatcher.instance.registerObject(this);
    }

    @Override
    public void smsReceived(SmsMessage message) {
        list.add(message);
        System.out.println("Message Received: " + message);
    }

    @Override
    public void smsSent(SmsMessage message) {
        list.add(message);
        System.out.println("Message sent: " + message);
    }
}
