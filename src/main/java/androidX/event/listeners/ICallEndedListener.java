package androidX.event.listeners;

/**
 * Created by amen on 9/16/17.
 */
public interface ICallEndedListener {
    void callEnded(int callId);
}
