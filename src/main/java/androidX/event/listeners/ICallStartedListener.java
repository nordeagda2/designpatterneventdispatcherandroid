package androidX.event.listeners;

/**
 * Created by amen on 9/16/17.
 */
public interface ICallStartedListener {
    void callStarted(int callId);
    void outgoingCallStarted(int callId);
}
