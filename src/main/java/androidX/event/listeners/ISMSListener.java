package androidX.event.listeners;

import androidX.event.SmsMessage;

/**
 * Created by amen on 12/17/17.
 */
public interface ISMSListener {
    public void smsReceived(SmsMessage message);

    public void smsSent(SmsMessage message);
}
