package androidX.event;

import java.time.LocalDateTime;

/**
 * Created by amen on 9/18/17.
 */
public class CallEntry {
    private int call_id;
    private LocalDateTime time_started;
    private LocalDateTime time_ended;
    private CallType type;

    public CallEntry(int call_id, LocalDateTime time_started, CallType type) {
        this.call_id = call_id;
        this.time_started = time_started;
        this.type = type;
    }

    public CallEntry(int call_id, LocalDateTime time_started) {
        this.call_id = call_id;
        this.time_started = time_started;
        this.type = CallType.INCOMING;
    }

    public int getCall_id() {
        return call_id;
    }

    public void setCall_id(int call_id) {
        this.call_id = call_id;
    }

    public LocalDateTime getTime_started() {
        return time_started;
    }

    public void setTime_started(LocalDateTime time_started) {
        this.time_started = time_started;
    }

    public LocalDateTime getTime_ended() {
        return time_ended;
    }

    public void setTime_ended(LocalDateTime time_ended) {
        this.time_ended = time_ended;
    }
}
