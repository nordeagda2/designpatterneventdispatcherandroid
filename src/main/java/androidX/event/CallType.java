package androidX.event;

/**
 * Created by amen on 9/18/17.
 */
public enum CallType {
    OUTGOING, INCOMING
}
